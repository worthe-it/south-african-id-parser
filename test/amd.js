'use strict';

var path = require('path');
var mocha = require('mocha');
var describe = mocha.describe;
var it = mocha.it;
var expect = require('chai').expect;
const requirejs = require('requirejs');

// node require isn't provided here, so it has to use the AMD import option to
// import it.
requirejs.config({
  baseUrl: path.join(__dirname, "..")
});

describe('Asynchronous Module Definition', function () {
  it('can import the module using RequireJS', function() {
    var saIdParser = requirejs('south-african-id-parser.js');
    var validIdNumber = '1012311412187';
    expect(saIdParser.validate(validIdNumber)).to.equal(true);
  });
});
