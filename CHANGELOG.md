# Changelog

All notable changes to this project will be documented in this file.

This changelog focusses on changes which affect production users of this
library. It will not mention any changes to code structure, internal design
details, documentation, or testing unless they have some affect on the public
API.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2023-08-07

### Added

- The package now includes typescript type definitions.

### Breaking Changes

- Updated license to be dual licensed MIT or Apache 2.0. This isn't intended to
  restrict any use cases, I'm just standardising my licensing across multiple
  projects.

## [1.1.0] - 2023-06-13

### Added

- This changelog can now be used to track changes to the library's public API.
- Dependencies are now checked for security vulnerabilities using `npm audit`
  before release.
- API docs are now available at
  https://worthe-it.codeberg.page/south-african-id-parser/.

### Changed

- Code hosting has been moved to
  [Codeberg](https://codeberg.org/worthe-it/south-african-id-parser). Please use
  Codeberg's issue tracker for issue reporting.

## [1.0.0] - 2017-06-24

### Added

- South African ID number parsing and validation.

[unreleased]: https://codeberg.org/worthe-it/south-african-id-parser/compare/v2.0.0...HEAD
[2.0.0]: https://codeberg.org/worthe-it/south-african-id-parser/releases/tag/v2.0.0
[1.1.0]: https://codeberg.org/worthe-it/south-african-id-parser/releases/tag/v1.1.0
[1.0.0]: https://codeberg.org/worthe-it/south-african-id-parser/releases/tag/v1.0.0
