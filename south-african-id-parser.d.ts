export function parse(idNumber: string): {
    /**
     * - true
     */
    isValid: boolean;
    /**
     * - The date of birth from the ID number.
     */
    dateOfBirth: Date;
    /**
     * - The sex from the ID number - true if male, false if female.
     */
    isMale: boolean;
    /**
     * - The sex from the ID number - true if female, false if male.
     */
    isFemale: boolean;
    /**
     * - Citizenship status from the ID
     * number, true if it indicates South African citizenship.
     */
    isSouthAfricanCitizen: boolean;
} | {
    /**
     * - false
     */
    isValid: boolean;
};
export function validate(idNumber: string): boolean;
export function parseDateOfBirth(idNumber: string): Date;
export function parseIsMale(idNumber: string): boolean;
export function parseIsFemale(idNumber: string): boolean;
export function parseIsSouthAfricanCitizen(idNumber: string): boolean;
//# sourceMappingURL=south-african-id-parser.d.ts.map