# Contributing

## Development Guide

### Setting up a dev environment

Install the following through your system's package manager or by visiting the
project's website.

- [NodeJS](https://nodejs.org) 18 (LTS) or newer
- npm 8 or newer (note that this is generally bundled with Node

### Building and running tests

Dependencies are fetched with NPM.

```sh
npm install
```

After fetching dependencies, tests can also be run using NPM. This includes both
linting and unit tests.

```sh
npm test
```

## Submitting contributions

If you have a change you'd like to contribute upstream, please open a pull
request on
[Codeberg](https://codeberg.org/worthe-it/south-african-id-parser/pulls).

## I Have a Question

If you have a question which isn't answered by any of the existing
documentation, please open an
[issue](https://codeberg.org/worthe-it/south-african-id-parser/issues).

Please search the issues to see if anyone else has already asked the same
question before opening a new issue.
