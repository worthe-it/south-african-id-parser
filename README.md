# South African ID Number Parser

[![status-badge](https://ci.codeberg.org/api/badges/worthe-it/south-african-id-parser/status.svg)](https://ci.codeberg.org/worthe-it/south-african-id-parser)
[![npm](https://img.shields.io/npm/v/south-african-id-parser)](https://www.npmjs.com/package/south-african-id-parser)

The ID Numbers issued in South Africa follow a regular format that you
can use to derive some information about them. The following
information is available:

* Date of Birth
* Sex
* Citizenship

This library can also check if the ID number supplied is a valid South
African ID number.

More information on the ID number format can be found
[here](https://web.archive.org/web/20200101101847/http://geekswithblogs.net/willemf/archive/2005/10/30/58561.aspx)
and [here](http://knowles.co.za/generating-south-african-id-numbers/).

## API Docs

API docs are available [here](https://worthe-it.codeberg.page/south-african-id-parser/);

## Usage

Download the library from NPM using the following command in a terminal:

```sh
npm install --save south-african-id-parser
```

### Usage In NodeJS

```js
var saIdParser = require('south-african-id-parser');
var info = saIdParser.parse('9001049818080');
```

### Usage In the Browser

When used in the browser, the library will add the `saIdParser` object
to the window for you to use.

```html
<script src="south-african-id-parser.js"></script>
<script>
    var info = saIdParser.parse('9001049818080');
</script>
```

### Parse Everything

The package exposes the `.parse(idNumber)` method for calling all of
the validation and parsing in one.

If validation fails, the resulting object only has the isValid property.

```js
var saIdParser = require('south-african-id-parser');
var validIdNumber = '9001049818080';

var info = saIdParser.parse(validIdNumber);
// info === {
//   isValid: true,
//   dateOfBirth: new Date(1990, 0, 4),
//   isMale: true,
//   isFemale: false,
//   isSouthAfricanCitizen: true
// }

var invalidIdNumber = '1234567';
info = saIdParser.parse(invalidIdNumber);
// info === {
//   isValid: false
// }
```

### Only Validate

`.validate(idNumber)` only checks if the ID number is valid.

```js
var saIdParser = require('south-african-id-parser');
var validIdNumber = '9001049818080';
var isValid = saIdParser.validate(validIdNumber);

// valid === true
```

### Only Parse Date of Birth

The method does not do a full validation on the ID number, but it will
return undefined if either the number supplied is not a 13 digit
number string or the date section of the ID number is invalid.

```js
var saIdParser = require('south-african-id-parser');
var validIdNumber = '9001049818080';
var dateOfBirth = saIdParser.parseDateOfBirth(validIdNumber);

// dateOfBirth === new Date(1990, 0, 4)
```

The date of birth included in the ID number has a two digit year. For
example, 90 instead of 1990.

This is handled by comparing the date of birth to the current date,
and choosing the century that gives the person the lowest age, while
still putting their age in the past.

For example, assuming that the current date is 10 December 2015. If
the date of birth parsed is 10 December 15, it will be interpreted as
10 December 2015. If, on the other hand, the date of birth is parsed
as 11 December 15, that will be interpreted as 10 December 1915.

### Only Parse Sex

The method does not do a full validation on the ID number, but it will
return undefined if the number supplied is not a 13 digit number
string.

```js
var saIdParser = require('south-african-id-parser');
var validIdNumber = '9001049818080';
var isFemale = saIdParser.parseIsFemale(validIdNumber);
var isMale = saIdParser.parseIsMale(validIdNumber);

// isFemale === false
// isMale === true
```

### Only Parse Citizenship

The method does not do a full validation on the ID number, but it will
return undefined if the number supplied is not a 13 digit number
string.

```js
var saIdParser = require('south-african-id-parser');
var validIdNumber = '9001049818080';
var isSouthAfricanCitizen = saIdParser.parseIsSouthAfricanCitizen(validIdNumber);

// isSouthAfricanCitizen === true
```

## Releases

See [CHANGELOG.md](https://codeberg.org/worthe-it/south-african-id-parser/src/branch/main/CHANGELOG.md)
for release notes.

Releases are also available on
[NPM](https://www.npmjs.com/package/south-african-id-parser) and
[Codeberg](https://codeberg.org/worthe-it/south-african-id-parser/releases).

## Reporting issues

Please report any issues on the project's [issue
tracker](https://codeberg.org/worthe-it/south-african-id-parser/issues).

## Roadmap

This library is considered to be feature complete, so there are no new planned
features.

This project is still actively maintained. Reported issues will be addressed,
and dependencies will be kept up to date.

## Contributing

See [CONTRIBUTING.md](https://codeberg.org/worthe-it/south-african-id-parser/src/branch/main/CONTRIBUTING.md)
for instructions on setting up a development environment to make changes and
submit contributions.

## Support

If you get value from this project, consider supporting me on
[Patreon](https://www.patreon.com/worthe_it). Support via Patreon will allow me
to spend more time writing and publishing open source software.

## License

Copyright 2015-2023, Justin Wernick.

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
